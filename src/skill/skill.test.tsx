import { render, screen, logRoles } from "@testing-library/react";
import { Skills } from "./Skill";
const skills = ["HTML", "CSS", "JS"];
describe("SkillPage", () => {
  test("render correctly", () => {
    render(<Skills skills={skills} />);
    const listElement = screen.getByRole("list");
    expect(listElement).toBeInTheDocument();
  });
  test("render listitem", () => {
    render(<Skills skills={skills} />);
    const listItemElement = screen.getAllByRole("listitem");
    expect(listItemElement).toHaveLength(skills.length);
  });
  test("render login btn", () => {
    render(<Skills skills={skills} />);
    const loginBtn = screen.getByRole("button", {
      name: "Login",
    });
    expect(loginBtn).toBeInTheDocument();
  });
  test("Start learning text is not rendered", () => {
    render(<Skills skills={skills} />);
    const textElement = screen.queryByRole("button", {
      // trả về null nếu không tìm thấy
      name: "Start Learning",
    });
    expect(textElement).not.toBeInTheDocument();
  });
  test("Start learning btn is eventually displayed", async () => {
    // const view = render(<Skills skills={skills} />);
    // logRoles(view.container);
    // screen.debug();
    render(<Skills skills={skills} />);
    const textElement = await screen.findByRole(
      "button",
      {
        name: "Start Learning",
      },
      {
        timeout: 2000,
      }
    );
    // screen.debug(); render ra cây dom

    expect(textElement).toBeInTheDocument();
  });
});
