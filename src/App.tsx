import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { Application } from "./application/application";
import { Skills } from "./skill/Skill";
import { Counter } from "./Counter/Counter";
import { AppProviders } from "./providers/app-provider";
import { MuiMode } from "./mui/MuiMode";

function App() {
  return (
    // <div className="App">
    //   <Application />
    //   <Skills skills={["html", "css"]} />
    //   <Counter />
    // </div>
    <AppProviders>
      <div className="App">
        <MuiMode />
      </div>
    </AppProviders>
  );
}

export default App;
