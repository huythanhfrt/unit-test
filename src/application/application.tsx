export const Application = () => {
  return (
    <>
      <h1>Application form</h1>
      <h2>Section</h2>
      <img src="" alt="reactjs" />
      <span title="ReactJS">ReactJS</span>
      <div data-testid="Custom a form">Custom a form</div>
      <form>
        <div>
          <label htmlFor="name">Name</label>
          <input
            type="text"
            id="name"
            placeholder="Fullname"
            value="John"
            onChange={() => {}}
          />
        </div>
        <div>
          <label htmlFor="job-location">Job location</label>
          <select id="job-location">
            <option value="">Select a country</option>
            <option value="US">United States</option>
            <option value="UK">United Kingdom</option>
            <option value="CA">Canada</option>
            <option value="IN">India</option>
            <option value="AU">Australia</option>
          </select>
        </div>
        <div>
          <label>
            <input type="checkbox" name="" id="terms" /> I agree to the terms
            and conditions
          </label>
          <button disabled>Submit</button>
        </div>
      </form>
    </>
  );
};
