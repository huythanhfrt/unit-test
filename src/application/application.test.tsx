import { render, screen } from "@testing-library/react";
import { Application } from "./application";

describe("render", () => {
  test("application correctly", () => {
    render(<Application />);
    const heading1Element = screen.getByRole("heading", {
      level: 1,
    });
    expect(heading1Element).toBeInTheDocument();
    const heading2Element = screen.getByRole("heading", {
      level: 2,
    });
    expect(heading2Element).toBeInTheDocument();

    const labelText = screen.getByLabelText("Name");
    expect(labelText).toBeInTheDocument();

    const divElement = screen.getByTestId("Custom a form"); // data-testid
    expect(divElement).toBeInTheDocument();

    const spanElement = screen.getByTitle("ReactJS");
    expect(spanElement).toBeInTheDocument();

    const imgElement = screen.getByAltText("reactjs");
    expect(imgElement).toBeInTheDocument();

    const nameElement = screen.getByRole("textbox");
    expect(nameElement).toBeInTheDocument();

    const nameElement2 = screen.getByDisplayValue("John");
    expect(nameElement2).toBeInTheDocument();

    const inputElement = screen.getByPlaceholderText("Fullname");
    expect(inputElement).toBeInTheDocument();

    const jobLocationElement = screen.getByRole("combobox");
    expect(jobLocationElement).toBeInTheDocument();

    const termsElement = screen.getByRole("checkbox");
    expect(termsElement).toBeInTheDocument();

    const submitButtonElement = screen.getByRole("button");
    expect(submitButtonElement).toBeInTheDocument();
    expect(submitButtonElement).toBeDisabled();
  });
});
