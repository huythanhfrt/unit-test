import { render, screen } from "@testing-library/react";
import DemoComponent from "./DemoComponent";

describe("render", () => {
  test("Component correcasdtly adsasdf", () => {
    render(<DemoComponent />);
    const content = screen.getByText(/democomponent/i);
    expect(content).toBeInTheDocument(); //toBe là matcher function
  });
  test("DemoComponent with props is name", () => {
    render(<DemoComponent name="thành" />);
    const content = screen.getByText(`DemoComponent by thành`);
    expect(content).toBeInTheDocument();
    const divElement = screen.getByText((content) =>
      content.startsWith("DemoComponent")
    );
    expect(divElement).toBeInTheDocument();
  });
  test("DemoComponent function", () => {
    render(<DemoComponent name="thành" />);
    const divElement = screen.getByText((content) =>
      content.startsWith("DemoComponent")
    );
    expect(divElement).toBeInTheDocument();
  });
});
// test("renders learn react link", () => {
//   render(<App />);
//   const linkElement = screen.getByText(/learn react/i);
//   expect(linkElement).toBeInTheDocument();
// });
// test("render demoComponent", () => {
//   render(<DemoComponent />);
//   const content = screen.getByText(/DemoComponent/i);
//   expect(content).toBeInTheDocument();
// });
