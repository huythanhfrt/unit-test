import { DemoComponentProps } from "./propsInterface.types";

export default function DemoComponent(props: DemoComponentProps) {
  return <div>DemoComponent by {props.name}</div>;
}
