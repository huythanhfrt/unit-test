import { render, screen } from "@testing-library/react";
import { Counter } from "./Counter";
import user from "@testing-library/user-event";
describe("Counter", () => {
  test("render correctly", () => {
    render(<Counter />);
    const headingElement = screen.getByRole("heading");
    expect(headingElement).toBeInTheDocument();
    const btnElement = screen.getByRole("button", {
      name: "Increment",
    });
    expect(btnElement).toBeInTheDocument();
  });
  test("render counter = 1 when user click the btn", async () => {
    user.setup();
    render(<Counter />);
    const btnElement = screen.getByRole("button", {
      name: "Increment",
    });
    await user.click(btnElement);
    const headingElement = screen.getByRole("heading");
    expect(headingElement).toHaveTextContent("1");
  });
  test("render a count of 10 after clicking the set btn", async () => {
    user.setup();
    render(<Counter />);
    const amountInput = screen.getByRole("spinbutton");
    await user.type(amountInput, "10");
    expect(amountInput).toHaveValue(10);
    const setBtn = screen.getByRole("button", {
      name: "Set",
    });
    await user.click(setBtn);
    const countElement = screen.getByRole("heading");
    expect(countElement).toHaveTextContent("10");
  });
  test("elements are focused in the right order", async () => {
    user.setup();
    render(<Counter />);
    const amountInput = screen.getByRole("spinbutton");
    const setBtn = screen.getByRole("button", {
      name: "Set",
    });
    const btnElement = screen.getByRole("button", {
      name: "Increment",
    });
    await await user.tab();
    expect(btnElement).toHaveFocus();
    await await user.tab();
    expect(amountInput).toHaveFocus();
    await await user.tab();
    expect(setBtn).toHaveFocus();
  });
});
